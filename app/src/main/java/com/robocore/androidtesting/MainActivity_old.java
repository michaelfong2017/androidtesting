//package com.robocore.androidtesting;
//
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.util.Log;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//
//import com.robocore.objectfollowing.objectdetection.listeners.OnObjectDetectedListener;
//import com.robocore.objectfollowing.objectdetection.services.ObjectDetectionService;
//import com.robocore.objectfollowing.objectdetection.tflite.Classifier;
//import com.robocore.objectfollowing.secretcamera.listeners.OnImageProcessedListener;
//import com.robocore.objectfollowing.secretcamera.services.CameraService;
//
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//
//public class MainActivity extends AppCompatActivity implements OnImageProcessedListener,
//        OnObjectDetectedListener {
//
//    private static final String TAG = MainActivity.class.getSimpleName();
//    // PERMISSION_REQUEST_CODE can be arbitrary.
//    private static final int PERMISSION_REQUEST_CODE = 102;
//
//    // secretcamera api
//    private CameraService cameraService;
//    private static String cameraID = "1";
//
//    // objectdetection api
//    private com.robocore.objectfollowing.objectdetection.services.ObjectDetectionService objectDetectionService;
//    private static final boolean USE_DISPLAYED_OBJECTS = true;
//    private static final Set<String> DISPLAYED_OBJECTS = new HashSet<>(Arrays.asList("person"));
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        Intent intent = new Intent(this, com.robocore.objectfollowing.secretcamera.activities.CheckPermissionsActivity.class);
//        startActivityForResult(intent, PERMISSION_REQUEST_CODE);
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        if (cameraService != null) {
//            cameraService.closeSecretCamera();
//        }
//        if (objectDetectionService != null) {
//            objectDetectionService.stopObjectDetection();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        switch (requestCode) {
//            case PERMISSION_REQUEST_CODE:
//                if (resultCode == RESULT_OK) {
//                    cameraService = new CameraService(this);
//                    cameraService.setCamera(cameraID);
//                    cameraService.setOnImageProcessedListener(this);
//                    cameraService.startSecretCamera();
//                }
//                break;
//        }
//    }
//
//    @Override
//    public void onImageProcessed(Bitmap imageBitmap) {
//        Log.d(TAG, "imageBitmap: " + imageBitmap);
//        objectDetectionService = new ObjectDetectionService(this);
//        objectDetectionService.setCameraID("1");
//        objectDetectionService.setUseDisplayedObjects(true);
//        objectDetectionService.setOnObjectDetectedListener(this);
//        objectDetectionService.startObjectDetection(imageBitmap);
//    }
//
//    @Override
//    public void onObjectDetected(List<Classifier.Recognition> objectDetectionResults) {
//        Log.d(TAG, "objectDetectionResults: " + objectDetectionResults);
//    }
//}