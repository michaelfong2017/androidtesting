package com.robocore.androidtesting;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.robocore.objectfollowing.objectdetection.services.ObjectDetectionService;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private ObjectDetectionService objectDetectionService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}