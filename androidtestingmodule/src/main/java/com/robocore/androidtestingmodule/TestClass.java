package com.robocore.androidtestingmodule;

import android.util.Log;

public class TestClass {
    private static String TAG = TestClass.class.getSimpleName();
    private String init = "not yet initialized";

    public static void Test1() {
        Log.d(TAG, "Test1()");
    }

    public void Test2() {
        Log.d(TAG, "Test2()");
    }

    public void Init() {
        init = "initialized";
    }

    public void PrintInit() {
        Log.d(TAG, init);
    }
}
